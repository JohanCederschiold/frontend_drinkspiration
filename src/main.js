import Vue from 'vue'
import App from './App.vue'
import BootstrapVue from 'bootstrap-vue'
import VueSuggestion from 'vue-suggestion'
import Header from './components/shared/Header.vue'
import Cocktail from './components/shared/Cocktail.vue'
import Cocktails from './components/AllCocktails.vue'
import Add from './components/AddCocktail.vue'
import Surprice from './components/randomcocktail.vue'
import List from './components/List.vue'
import Search from './components/Search.vue'
import Home from './components/Home.vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueRouter from 'vue-router'
import Vuex from 'vuex'

Vue.config.productionTip = false

Vue.use(BootstrapVue)
Vue.use(VueSuggestion)
Vue.use(VueRouter)
Vue.use(Vuex)

Vue.component('app-header', Header)
Vue.component('app-cocktail', Cocktail)
const cocktails = Vue.component('app-cocktails', Cocktails)
const addCocktail = Vue.component('app-addcocktail', Add)
const random = Vue.component('app-random', Surprice)
const list = Vue.component('app-list', List)
const search = Vue.component('app-search', Search)
const home = Vue.component('app-home', Home)



export const eventBus = new Vue({
  data: {
    drinksList: [],
    endpoint: 'http://DrinkspirationLoadbalancer2-902389930.eu-north-1.elb.amazonaws.com:8080'
  }, 
  methods: {
    addDrinkToList(id) {
      this.drinksList.push(id)
    }
  }
})



export const router = new VueRouter({
  routes: [
    {
      component: cocktails,
      path: '/cocktails'
    },
    {
      component: addCocktail,
      path: '/add'
    },
    {
      component: random,
      path: '/random'
    },
    {
      component: list,
      path: '/list'
    }, 
    {
      component: search,
      path: '/search'
    }, 
    {
      component: home,
      path: '/'
    }
  ]
})

export const store = new Vuex.Store({
  state: {
    testValue : false,
    chosenDrinks : []
  }, 
  mutations: {
    testMethod(state, newValue) {
      state.testValue = newValue
    },
    addChosen(state, id) {
      state.chosenDrinks.push(id)
    },
    removeChosen(state, id) {
      let index = state.chosenDrinks.indexOf(id)
      state.chosenDrinks.splice(index, 1)
    }
  }
})


new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')


